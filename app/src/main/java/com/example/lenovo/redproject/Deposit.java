package com.example.lenovo.redproject;

public class Deposit {
    double depositSum;
    double rate;
    int timeInMonth;
    boolean hasCapitalization;
    double everyMonthReplenishment;
    double tax;

    public Deposit(double depositSum, double rate, int timeInMonth, boolean hasCapitalization, double everyMonthReplenishment, double tax) {
        this.depositSum = depositSum;
        this.rate = rate;
        this.timeInMonth = timeInMonth;
        this.hasCapitalization = hasCapitalization;
        this.everyMonthReplenishment = everyMonthReplenishment;
        this.tax = tax;
    }

    public double calcResultSum(){
        double result = depositSum;
        for(int i = 0; i < timeInMonth; i++) {

            double rising = result*rate*(1-tax);
            if(hasCapitalization) {
                result += rising;
            }
            result += everyMonthReplenishment;
        }
        return result;
    }
    public double calcTotalReplenishment(){
        return everyMonthReplenishment*timeInMonth;
    }
    public double calcRiseSum(){
        double result = depositSum;
        for(int i = 0; i < timeInMonth; i++) {

            double rising = result*rate*(1-tax);
            if(hasCapitalization) {
                result += rising;
            }
            result += everyMonthReplenishment;
        }
        return result - depositSum;
    }

    public double calcEveryMonthSum(){
        if(!hasCapitalization) {
            return depositSum * rate * (1 - tax);
        }else{
            double result = depositSum;
            for(int i = 0; i < timeInMonth; i++) {

                double rising = result*rate*(1-tax);
                if(hasCapitalization) {
                    result += rising;
                }
                result += everyMonthReplenishment;
            }
            return (result - depositSum)/timeInMonth;
        }
    }
    public double getDepositSum() {
        return depositSum;
    }

    public int getTimeInMonth() {
        return timeInMonth;
    }

    public void setDepositSum(double depositSum) {
        this.depositSum = depositSum;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public void setTimeInMonth(int timeInMonth) {
        this.timeInMonth = timeInMonth;
    }

    public void setHasCapitalization(boolean hasCapitalization) {
        this.hasCapitalization = hasCapitalization;
    }

    public void setEveryMonthReplenishment(double everyMonthReplenishment) {
        this.everyMonthReplenishment = everyMonthReplenishment;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }
}
