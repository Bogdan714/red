package com.example.lenovo.redproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.sum)
    EditText sum;

    @BindView(R.id.rate)
    EditText rate;

    @BindView(R.id.term)
    EditText term;

    @BindView(R.id.replenishment)
    EditText replenishment;

    @BindView(R.id.tax)
    EditText tax;

    @BindView(R.id.capitalizm)
    CheckBox capitalizm;

    //NEXT - TextView
    @BindView(R.id.getNMonth)
    TextView getNMonth;

    @BindView(R.id.allAddSum)
    TextView allAddSum;

    @BindView(R.id.endingSum)
    TextView endingSum;

    @BindView(R.id.nMonth)
    TextView nMonth;

    @BindView(R.id.getMonth)
    TextView getMonth;


    Deposit deposit = new Deposit(0, 0, 0, false, 0, 0 );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }


    @OnTextChanged({R.id.sum, R.id.rate, R.id.term, R.id.replenishment, R.id.tax, R.id.capitalizm})
    public void onChange() {
        deposit.setDepositSum(Double.parseDouble(sum.getText().toString()));
        deposit.setRate(Double.parseDouble(rate.getText().toString()) / 100);
        deposit.setTimeInMonth(Integer.parseInt(term.getText().toString()));
        deposit.setEveryMonthReplenishment(Double.parseDouble(replenishment.getText().toString()) / 100);
        deposit.setTax(Double.parseDouble(tax.getText().toString()));
        deposit.setHasCapitalization(capitalizm.isChecked());

        getNMonth.setText(String.valueOf(deposit.calcEveryMonthSum()));
        allAddSum.setText(String.valueOf(deposit.calcTotalReplenishment()));
        endingSum.setText(String.valueOf(deposit.calcResultSum()));
        nMonth.setText(String.valueOf(deposit.getTimeInMonth()));
        getMonth.setText(String.valueOf(deposit.calcRiseSum()));

    }

}
